// cs174, assignment 1 sample file (RIGHT HANDED)

camera {
  location  <0, 0, 14>
  up        <0, 1,  0>
  right     <1.333333, 0,  0>
  look_at   <0, 0, 0>
}


light_source {<0, 0, 14> color rgb <1.5, 1.5, 1.5>}

sphere { <0, 0, 0>, 2 
      pigment { color rgb <1.0, 0.2, 1.0>}
      finish {ambient 0.2 diffuse 0.0 specular 0.6}
      translate <0, 0, 0>
}

sphere { <0, 0, 0>, 2 
      pigment { color rgb <0.1, 1.0, 1.0, 0.6>}
      finish {ambient 0.2 diffuse 0.0 specular 0.6 }
      translate <4, 0, 0>
}

plane {<0, 1, 0>, -3 
      pigment {color rgb <0.8, 0.2, 0.2>}
      finish {ambient 0.2 diffuse 0.0 specular 0.6}
}

