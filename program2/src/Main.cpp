/*
 * Main.cpp
 *
 * Main file for povray parser
 */

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sys/time.h>

#include "glm/glm.hpp"
#include "SceneObj.h"
#include "Shapes.h"
#include "Finish.h"
#include "Calculate.cuh"
#include "Image.h"
#include "BVH.h"
#include "KDTreeBuilder.h"
#include "types.h"

using namespace std;
using namespace glm;

int g_width;
int g_height;

vector<GeomObj*> g_objects;
vector<GeomObj*> g_planes;
vector<LightSource*> g_lights;
Camera *g_camera;
e_shading shadeType;

void parseInputFile(char *filename) {
   ifstream inputStream;
   inputStream.open(filename);

   if (!inputStream.is_open()) {
      printf("Could not open file %s.\n",filename);
      exit(1);
   }

   while (!inputStream.eof()) {
      string word;
      inputStream >> word;
      if (inputStream.eof()) {
         break;
      }
      if (word.substr(0, 2).compare("//") == 0) {
         inputStream.ignore(512,(int)'\n');
         continue;
      }
      else if (word.compare("camera") == 0) {
         g_camera = new Camera();
         g_camera->read(inputStream);
      }
      else if (word.compare("light_source") == 0) {
         LightSource *lightSource = new LightSource();
         lightSource->read(inputStream);
         g_lights.push_back(lightSource);
      }
      else if (word.compare("box") == 0) {
         Box *box = new Box();
         box->read(inputStream);
         g_objects.push_back(box);
      }
      else if (word.compare("sphere") == 0) {
         Sphere *sphere = new Sphere();
         sphere->read(inputStream);
         g_objects.push_back(sphere);
      }
      else if (word.compare("cone") == 0) {
         Cone *cone = new Cone();
         cone->read(inputStream);
         g_objects.push_back(cone);
      }
      else if (word.compare("plane") == 0) {
         Plane *plane = new Plane();
         plane->read(inputStream);
         g_planes.push_back(plane);
      }
      else if (word.compare("triangle") == 0) {
         Triangle *triangle = new Triangle();
         triangle->read(inputStream);
         g_objects.push_back(triangle);
      }
      else {
         cout << "Unknown item: " << word << endl;
      } 
   }
}

int main(int argc, char *argv[]) {
   
   double t1_total, t2_total, t1_inc, t2_inc;
   struct timeval t_total;

   gettimeofday(&t_total, NULL);  
   t1_total = t_total.tv_sec+(t_total.tv_usec/1000000.0);
   t1_inc = t_total.tv_sec+(t_total.tv_usec/1000000.0);

   if (argc < 5) {
      printf("Unrecognized or missing arguments.  Use format:\n\traytrace <width> <height> -I <input file> [-g]\n"); 
      exit(1);
   }

   g_width = atoi(argv[1]);
   g_height = atoi(argv[2]);

   if (g_width <= 0 || g_height <= 0) {
      printf("Image dimensions must be positive integers\n");
      exit(1);
   }

   printf("parse...\n");
   parseInputFile(argv[4]);

   gettimeofday(&t_total, NULL);  
   t2_inc = t_total.tv_sec+(t_total.tv_usec/1000000.0);

   printf("Parse time:  %.6lf s\n", t2_inc-t1_inc);
   t1_inc = t2_inc;

   shadeType = BLINN;
   if (argc > 5) {
      string word = argv[5];
      if (word.compare("--gaussian") == 0 || word.compare("-g") == 0)
         shadeType = GAUSSIAN;
   }

   printf("copy geometry...\n");
   vector<geom_t> geometry;
   vector<light_t> lights;
   camera_t cam;

   int planesIndex, i;
   for (i=0; i < g_objects.size(); i++) {
      geometry.push_back(g_objects[i]->getStruct());
   }
   planesIndex = i;
   for (i=0; i < g_lights.size(); i++) {
      lights.push_back(g_lights[i]->getStruct());
   }
   cam = g_camera->getStruct();

   gettimeofday(&t_total, NULL);  
   t2_inc = t_total.tv_sec+(t_total.tv_usec/1000000.0);

   printf("Copy time:  %.6lf s\n", t2_inc-t1_inc);
   t1_inc = t2_inc;

   printf("build bvh...\n");
   BVH *bvhBuilder = new BVH();
   vector<bvh_node_t> bvh = bvhBuilder->generateBVH(g_objects);

   for (i=0; i < g_planes.size(); i++) {
      geometry.push_back(g_planes[i]->getStruct());
   }

   gettimeofday(&t_total, NULL);  
   t2_inc = t_total.tv_sec+(t_total.tv_usec/1000000.0);

   printf("BVH time:  %.6lf s\n", t2_inc-t1_inc);

   printf("render...\n");
   createImage(g_width, g_height, geometry, bvh, planesIndex, lights, cam, shadeType);

   gettimeofday(&t_total, NULL);  
   t2_total = t_total.tv_sec+(t_total.tv_usec/1000000.0);  

   printf("Total time (including memory operations): %.6lf s\n", t2_total-t1_total);
   printf("Size of the object array (in bytes): %d\n", sizeof(geom_t) * geometry.size());
   printf("Size of the bvh array (in bytes): %d\n", sizeof(bvh_node_t) * bvh.size());

   return 0;
}
