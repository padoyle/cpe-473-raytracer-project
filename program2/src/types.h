#ifndef __TYPES_H__
#define __TYPES_H__

#include "glm/glm.hpp"
#include "Finish.h"

enum e_shape {
   BOX,
   SPHERE,
   CONE,
   PLANE,
   TRIANGLE
};

enum e_shading {
   BLINN,
   GAUSSIAN
};

typedef struct ray_struct {
   glm::vec3 p0;
   glm::vec3 d;
} ray_t;

typedef struct geom_struct {
   e_shape shape; 
   glm::vec3 v1;
   glm::vec3 v2;
   glm::vec3 v3;
   float s1;
   float s2;
   finish_t finish;
   glm::vec4 col;
   glm::mat4 invTrans; 
} geom_t;

typedef struct camera_struct {
   glm::vec3 pos;
   glm::vec3 up;
   glm::vec3 right;
   glm::vec3 look;
} camera_t;

typedef struct light_struct {
   glm::vec3 pos;
   glm::vec4 color;
} light_t;

typedef struct hit_struct {
   int index;
   float t;
} hit_t;

typedef struct bvh_node_struct {
   glm::vec3 bbMin;
   glm::vec3 bbMax;
   int obj;
   int left;
   int right;
} bvh_node_t;

typedef struct geom_data_struct {
   geom_t *geom;
   bvh_node_t *bvh;
   int count;
   int planesIndex;
   e_shading shadeType;
} gd_t;

typedef struct light_data_struct {
   light_t *lights;
   int count;
} ld_t;

typedef struct photon_struct {
   glm::vec3 pos;
   glm::vec3 dir;
   glm::vec3 col;
   short flag;
} photon_t;

typedef struct kd_node_struct {
   int dim;
   int photonId;
   int left;
   int right;
} kd_node_t;

#endif
