/*
 * Shapes.h
 *
 * Data objects for each of the supported shapes in the povray format
 */

#ifndef _SHAPES_H_
#define _SHAPES_H_

#include "glm/glm.hpp"
#include "SceneObj.h"
#include <string>
#include <fstream>
#include <iostream>
#include "Finish.h"

using namespace std;
using namespace glm;

class Box : public GeomObj {
   public: 
      vec3 pos2;
      Box();
      ~Box();
      void read(ifstream &input);
      virtual geom_t getStruct();
      virtual bvh_node_t getBVHNode();
};

class Sphere : public GeomObj {
   public:
      float rad;
      Sphere();
      ~Sphere();
      void read(ifstream &input);
      virtual geom_t getStruct();
      virtual bvh_node_t getBVHNode();
};

class Cone : public GeomObj {
   public:
      float r1;
      vec3 pos2;
      float r2;
      Cone();
      ~Cone();
      void read(ifstream &input);
      virtual geom_t getStruct();
      virtual bvh_node_t getBVHNode();
};

class Plane : public GeomObj {
   public:
      vec3 norm;
      float d;
      Plane();
      ~Plane();
      void read(ifstream &input);
      virtual geom_t getStruct();
      virtual bvh_node_t getBVHNode();
};

class Triangle : public GeomObj {
   public:
      vec3 pos2;
      vec3 pos3;
      Triangle();
      ~Triangle();
      void read(ifstream &input);
      virtual geom_t getStruct();
      virtual bvh_node_t getBVHNode();
};

#endif
