/**
 * BVH
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>

#include "SceneObj.h"
#include "types.h"
#include "BVH.h"

using namespace std;

bool compareX (bvh_node_t A, bvh_node_t B) {
   return (A.bbMin.x < B.bbMin.x);
}

bool compareY (bvh_node_t A, bvh_node_t B) {
   return (A.bbMin.y < B.bbMin.y);
}

bool compareZ (bvh_node_t A, bvh_node_t B) {
   return (A.bbMin.z < B.bbMin.z);
}

BVH::BVH() {

}

vector<bvh_node_t> BVH::generateBVH(const vector<GeomObj*>& geometry) {
   vector<bvh_node_t> bvh;
   vector<bvh_node_t> leaves;

   // Build a list of shapes first, represented as nodes with shape list references
   for (int i=0; i < geometry.size(); i++) {
      bvh_node_t node = geometry[i]->getBVHNode();
      node.obj = i;
      leaves.push_back(node);
   }

   buildTree(bvh, leaves);
   //printTree(bvh, 0, 0);

   return bvh; 
}

int BVH::buildTree(vector<bvh_node_t>& bvh, vector<bvh_node_t>& shapes) {
   // The size is the next index that will be pushed on; this node's index-to-be
   int myIndex = bvh.size(); 
   bvh_node_t node = buildNode(shapes); 
   bvh.push_back(node);
   // Base case:
   if (shapes.size() == 1) {
      return myIndex;
   }
   // Recursive case:
   // Split into two sorted lists
   int axis = longestAxis(shapes);
   switch (axis) {
   case 0:
      sort(shapes.begin(), shapes.end(), compareX); 
      break;
   case 1:
      sort(shapes.begin(), shapes.end(), compareY);
      break;
   case 2:
      sort(shapes.begin(), shapes.end(), compareZ);
      break;
   }
   vector<bvh_node_t> leftList, rightList;
   for (int i=0; i < shapes.size()/2; i++) {
      leftList.push_back(shapes[i]);
   }
   for (int i=shapes.size()/2; i < shapes.size(); i++) {
      rightList.push_back(shapes[i]);
   }
  
   // Recurse on left and right
   node.left = buildTree(bvh, leftList);
   node.right = buildTree(bvh, rightList);
   bvh[myIndex] = node;

   return myIndex;
}

bvh_node_t BVH::buildNode(const vector<bvh_node_t>& shapes) {
   if (shapes.size() == 1) {
      return shapes[0];
   }
   bvh_node_t node = {shapes[0].bbMin, shapes[0].bbMax, -1, -1, -1};
   for (int i=1; i < shapes.size(); i++) {
      // Loop over the dimensions
      for (int d=0; d < 3; d++) {
         // Set min if less than current
         if (shapes[i].bbMin[d] < node.bbMin[d])
            node.bbMin[d] = shapes[i].bbMin[d];

         // Set max if greater than current
         if (shapes[i].bbMax[d] > node.bbMax[d])
            node.bbMax[d] = shapes[i].bbMax[d];
      }
   }
   
   return node;
}

int BVH::longestAxis(const vector<bvh_node_t>& shapes) {
   int axis = 0;
   float axisLength = 0.f;   

   for (int dim=0; dim < 3; dim++) {
      float min = shapes[0].bbMin[dim];
      float max = shapes[0].bbMax[dim];
      for (int i=1; i < shapes.size(); i++) {
         if (shapes[i].bbMin[dim] < min) {
            min = shapes[i].bbMin[dim];
         } 
         if (shapes[i].bbMax[dim] > max) {
            max = shapes[i].bbMax[dim];
         } 
      }
      if (max - min > axisLength) {
         axisLength = max - min;
         axis = dim;
      }
   }
   return axis;
}

void BVH::printTree(const vector<bvh_node_t>& tree, int index, int depth) {
   for (int i=0; i < depth; i++) {
      printf("  ");
   } 
   printNode(tree[index]);
   if (tree[index].left != -1) {
      printTree(tree, tree[index].left, depth+1);
   }
   if (tree[index].right != -1) {
      printTree(tree, tree[index].right, depth+1);
   }
}

void BVH::printNode(bvh_node_t node) {
   printf("Min: <%.2f, %.2f, %.2f> Max: <%.2f, %.2f, %.2f> Obj: %d Left: %d, Right: %d\n",
         node.bbMin.x, node.bbMin.y, node.bbMin.z, node.bbMax.x, node.bbMax.y, node.bbMax.z,
         node.obj, node.left, node.right);
}

void BVH::printTreeLinear(const vector<bvh_node_t>& tree) {
   printf("\n");
   for (int i=0; i < tree.size(); i++) {
      printf("%d: ", i);
      printNode(tree[i]); 
   }
}
