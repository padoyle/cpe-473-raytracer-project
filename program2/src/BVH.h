/**
 * BVH
 *
 */

using namespace std;

bool compareX(bvh_node_t A, bvh_node_t B);
bool compareY(bvh_node_t A, bvh_node_t B);
bool compareZ(bvh_node_t A, bvh_node_t B);

class BVH {

   public:
      BVH();

      vector<bvh_node_t> generateBVH(const vector<GeomObj*>& geometry);
      void printTree(const vector<bvh_node_t>& tree, int index, int depth); 
      void printTreeLinear(const vector<bvh_node_t>& tree); 

   private:
      int buildTree(vector<bvh_node_t>& bvh, vector<bvh_node_t>& shapes);
      bvh_node_t buildNode(const vector<bvh_node_t>& shapes);
      int longestAxis(const vector<bvh_node_t>& shapes);
      void printNode(bvh_node_t node);
};
