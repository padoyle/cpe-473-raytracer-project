/*
 * Calculate.cu
 *
 * Perform cuda calculations for the image
 */

#include <stdio.h>
#include <sys/time.h>
#include <vector>

#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>

#include "glm/glm.hpp"

#include "SceneObj.h"
#include "Image.h"
#include "types.h"

#include "BVH.h"
#include "KDTreeBuilder.h"

#define BLOCK_SIZE 16
#define RECURSION_DEPTH_MAX 2
#define E_CONSTANT 2.71828f
#define EPSILON 0.001f
#define SAMPLE_SIZE 2
#define BVH_TRAV_STACK_SIZE 24
#define PHOTON_COUNT 500
#define PHOTON_BLOCK_WIDTH 16
#define PHOTON_BLOCK_HEIGHT 4
#define PI 3.1415926535897932384626433832795

/**
 * Function declarations
 */

__device__ float clamp (float value, float lower, float upper);
__device__ vec3 getNormal(vec3 pos, geom_t shp);
__device__ hit_t findIntersections(gd_t gdata, ray_t ray, float maxT);
__device__ hit_t intersectObject(geom_t obj, int objIndex, ray_t ray); 
__device__ ray_t getRefractionRay(geom_t shp, ray_t ray, vec3 N, vec3 poi); 
__device__ float getPlaneIntersection(geom_t plane, ray_t ray);
__device__ float getSphereIntersection(geom_t sphere, ray_t ray);
__device__ float getTriangleIntersection(geom_t tri, ray_t ray); 
__device__ float getBoxIntersection(geom_t box, ray_t ray); 
__device__ float getBoundingBoxIntersection(vec3 min, vec3 max, ray_t ray);
__device__ vec3 getShading(int shpIndex, gd_t gdata, ld_t ldata, ray_t ray, float t);
__device__ ray_t getRay(camera_t camera, int rx, int ry, int width, int height, curandState *state);

/**
 * Utility functions
 */

__device__ float clamp (float value, float lower, float upper) {
   value = (value < lower) ? lower : value;
   value = (value > upper) ? upper : value;
   return value;
}

__device__ vec3 getNormal(vec3 pos, geom_t shp) {
   vec3 N = vec3(0,0,0); 
   switch(shp.shape) {
   case SPHERE:
      pos = vec3(shp.invTrans * vec4(pos, 1.0f));
      N = pos - shp.v1; 
      break;
   case PLANE:
      N = shp.v1;
      break;
   case TRIANGLE:
      N = glm::cross(shp.v2 - shp.v1, shp.v3 - shp.v1);
      break;
   case BOX:
      pos = vec3(shp.invTrans * vec4(pos, 1.0f));
      if (abs(pos.x - shp.v1.x) < EPSILON) {
         N = vec3(-1.f, 0, 0);
      }
      else if (abs(pos.x - shp.v2.x) < EPSILON) {
         N = vec3(1.f, 0, 0);
      }
      else if (abs(pos.y - shp.v1.y) < EPSILON) {
         N = vec3(0, -1.f, 0);
      }
      else if (abs(pos.y - shp.v2.y) < EPSILON) {
         N = vec3(0, 1.f, 0);
      }
      else if (abs(pos.z - shp.v1.z) < EPSILON) {
         N = vec3(0, 0, -1.f);
      }
      else {
         N = vec3(0, 0, 1.f);
      }
   }

   N = glm::normalize(vec3(glm::transpose(shp.invTrans) * vec4(N,0.0f)));
   return N;
}

template <int depth>
__device__ vec3 rayColor(gd_t gdata, ld_t ldata, ray_t ray) {
   hit_t hit;
   vec3 color;
   vec3 N;
   vec3 poi;
   float rfl, rfr, ior;
   bool hasRfr;
   vec3 rflComp = vec3(0);
   vec3 rfrComp = vec3(0);
   int x = blockIdx.x * BLOCK_SIZE + threadIdx.x;
   int y = blockIdx.y * BLOCK_SIZE + threadIdx.y; 

   // Find a hit
   hit = findIntersections(gdata, ray, -1.f); 

   // If no hit found, return black
   if (hit.index < 0) {
      return vec3(0);
   }

   // Get the color at this point
   color = getShading(hit.index, gdata, ldata, ray, hit.t);

   rfl = gdata.geom[hit.index].finish.rfl;
   hasRfr = gdata.geom[hit.index].finish.rfr;
   if (hasRfr)
      rfr = gdata.geom[hit.index].col.w;
   else
      rfr = 0.f;
   ior = gdata.geom[hit.index].finish.ior;

   if (rfl == 0 && (!hasRfr || ior == 0)) {
      return color;
   }

   poi = ray.p0 + hit.t * ray.d;
   N = getNormal(poi, gdata.geom[hit.index]);

   //Get reflectance
   if (hasRfr && ior > 0) {
      float costheta, R, R0;
      if (glm::dot(-ray.d, N) < 0) {
         R0 = pow((ior-1.f)/(ior+1.f),2.f);
         costheta = glm::dot(ray.d, N); 
      }
      else {
         R0 = pow((1.f-ior)/(1.f+ior),2.f); 
         costheta = glm::dot(-ray.d, N); 
      }
      R = rfr * (R0 + (1.f - R0) * pow(1.f - costheta, 5.f));
      rfl = R;
      rfr = rfr - R;
   }
   //Get refraction
   if (hasRfr && ior > 0) {
      ray_t refr = getRefractionRay(gdata.geom[hit.index], ray, N, poi);
      if (glm::length (refr.d) != 0) {
         // This way the collisions should work out right
         refr.p0 += refr.d * EPSILON;
         rfrComp = rfr * rayColor<depth+1>(gdata, ldata, refr); 
         vec3 dir = -ray.d;
      }
   }
   //Get reflection
   if (rfl > 0) {
      ray_t refl;
      refl.p0 = poi;
      refl.d = ray.d - 2.0f * (glm::dot(ray.d, N)) * N; 
      refl.p0 += refl.d * EPSILON;
      rflComp = rfl * rayColor<depth+1>(gdata, ldata, refl);
   }
   return clamp(1.0f - rfl - rfr, 0.f, 1.f) * color + rflComp + rfrComp;
}

template<> 
__device__ vec3 rayColor<RECURSION_DEPTH_MAX>(gd_t gdata, ld_t ldata, ray_t ray) {
   return vec3(0);
}

__device__ ray_t getRefractionRay(geom_t shp, ray_t ray, vec3 N, vec3 poi) {
   ray_t refract = {poi, vec3(0,1,0)};
   float ratio;
   vec3 inc = ray.d;
   // For now, we'll choose ior's based on direction of normal
   if (glm::dot(N, -inc) < 0) {
      ratio = shp.finish.ior;
      N = -N;
   }
   else {
      ratio = 1.f / shp.finish.ior;
   }
   float internal = 1.f - (ratio * ratio) * (1.f - pow(glm::dot(inc, N),2.f));
   if (internal < 0) {
      //Total internal reflection: return zeroed out ray
      ray_t zeroRay = {vec3(0), vec3(0)};
      return zeroRay;
   }
   refract.d = glm::normalize(ratio * (inc - N * (glm::dot(inc, N))) - sqrt(internal) * N);
   return refract;
}

__device__ hit_t findIntersections(gd_t gdata, ray_t ray, float maxT) {
   hit_t bestHit = {-1, -1.f};

   for (int i=gdata.planesIndex; i < gdata.count; i++) {
      hit_t shpHit = intersectObject(gdata.geom[i], i, ray);

      // If valid t AND (min not yet set or t less than min), store and continue
      if (shpHit.t >= 0.f && (bestHit.t < 0.0f || shpHit.t < bestHit.t)) {
         if (maxT < 0.f) {
            bestHit = shpHit;
         }
         else if (shpHit.t >= 0.f && shpHit.t < maxT) {
            return shpHit;
         }
      }
   }

   int stack[BVH_TRAV_STACK_SIZE];
   int *stackPtr = stack;
   *stackPtr++ = -1;
   *stackPtr++ = 0;

   do {
      bvh_node_t node = gdata.bvh[*--stackPtr];
      // Get child nodes
      bvh_node_t nodeL = gdata.bvh[node.left];
      bvh_node_t nodeR = gdata.bvh[node.right];

      // Check for collisions with child nodes
      bool hasHitL = (getBoundingBoxIntersection(nodeL.bbMin, nodeL.bbMax, ray) >= 0);
      bool hasHitR = (getBoundingBoxIntersection(nodeR.bbMin, nodeR.bbMax, ray) >= 0);

      // Left node is leaf, check contained object
      if (hasHitL && nodeL.obj >= 0) {
         hit_t shpHit = intersectObject(gdata.geom[nodeL.obj], nodeL.obj, ray);

         // If valid t AND (min not yet set or t less than min), store and continue
         if (shpHit.t >= 0.f && (bestHit.t < 0.0f || shpHit.t < bestHit.t)) {
            if (maxT < 0.f) {
               bestHit = shpHit;
            }
            else if (shpHit.t >= 0.f && shpHit.t < maxT) {
               return shpHit;
            }
         }
      }
      // Right node is leaf, check contained object
      if (hasHitR && nodeR.obj >= 0) {
         hit_t shpHit = intersectObject(gdata.geom[nodeR.obj], nodeR.obj, ray);

         // If valid t AND (min not yet set or t less than min), store and continue
         if (shpHit.t >= 0.f && (bestHit.t < 0.0f || shpHit.t < bestHit.t)) {
            if (maxT < 0.f) {
               bestHit = shpHit;
            }
            else if (shpHit.t >= 0.f && shpHit.t < maxT) {
               return shpHit;
            }
         }
      }
      // Find out if we need to keep going
      bool travL = (hasHitL && nodeL.obj < 0); 
      bool travR = (hasHitR && nodeR.obj < 0); 

      if (travL) {
         *stackPtr++ = node.left;
      }
      if (travR) {
         *stackPtr++ = node.right;
      }
   } while (*(stackPtr-1) >= 0);

   return bestHit;
}

__device__ hit_t intersectObject(geom_t obj, int objIndex, ray_t ray) {
   mat4 trans = obj.invTrans;
   ray_t objRay = { vec3(trans * vec4(ray.p0, 1.0f)), vec3(trans * vec4(ray.d,0.0f)) };
   float t = -1.0f;
   
   switch (obj.shape) {
   case PLANE:
      t = getPlaneIntersection(obj, objRay); 
      break;
   case SPHERE:
      t = getSphereIntersection(obj, objRay);
      break;
   case TRIANGLE:
      t = getTriangleIntersection(obj, objRay);
      break;
   case BOX:
      t = getBoxIntersection(obj, objRay);
      break;
   }

   hit_t hit = {objIndex, t};
   return hit; 
}

/**
 * Intersection functions
 */

__device__ float getPlaneIntersection(geom_t plane, ray_t ray) {
   vec3 planePoint = normalize(plane.v1) * plane.s1; 
   vec3 normT = normalize(plane.v1);
   
   float num = dot((planePoint - ray.p0),normT);
   float denom = dot(ray.d, normT);
   if (denom == 0) {
      return -1;
   }

   return num / denom; 
}

__device__ float getSphereIntersection(geom_t sphere, ray_t ray) {
   vec3 pos = sphere.v1;
   float rad = sphere.s1;
   float A = glm::dot(ray.d, ray.d);
   // Since B would alwyas be multiplied by 2,
   // we can drop 2's and 4's throughout the formula
   float B = glm::dot(ray.d, (ray.p0 - pos));
   float C = glm::dot(ray.p0 - pos, ray.p0 - pos) - rad * rad;
   float disc = B * B - A * C;
   if (disc < 0) {
      return -1;
   }
   float radical = sqrt(disc);
   float t0 = (-B - radical) / (A); 
   float t1 = (-B + radical) / (A); 
   if (t0 >=0 || t1 >= 0) {
      vec3 poi = ray.p0 + t0 * ray.d;
   }
   return (t0 >= 0) ? t0 : t1;
}

__device__ float getTriangleIntersection(geom_t tri, ray_t ray) {
   mat3 A = mat3(1);
   A[0] = tri.v1 - tri.v2;
   A[1] = tri.v1 - tri.v3;
   A[2] = ray.d;
   mat3 betaMat = mat3(1); 
   betaMat[0] = tri.v1 - ray.p0;
   betaMat[1] = tri.v1 - tri.v3;
   betaMat[2] = ray.d;
   mat3 gammaMat = mat3(1); 
   gammaMat[0] = tri.v1 - tri.v2;
   gammaMat[1] = tri.v1 - ray.p0;
   gammaMat[2] = ray.d;
   mat3 tMat = mat3(1); 
   tMat[0] = tri.v1 - tri.v2;
   tMat[1] = tri.v1 - tri.v3;
   tMat[2] = tri.v1 - ray.p0;

   float detA = glm::determinant(A);
   float beta = glm::determinant(betaMat) / detA;
   float gamma = glm::determinant(gammaMat) / detA;
   float alpha = (1.f - beta - gamma);
   float t = glm::determinant(tMat) / detA;

   return (gamma > 0 && gamma < 1 && beta > 0 && beta < 1
         && beta + gamma < 1.f) ? t : -1.f;
}

__device__ float getBoundingBoxIntersection(vec3 min, vec3 max, ray_t ray) {
   float tMin = INT_MIN;
   float tMax = INT_MAX;
   for (int i=0; i < 3; i++) {
      if (ray.d[i] == 0.f) {
         if (ray.p0[i] < min[i] || ray.p0[i] > max[i]) {
            return -1.f;
         }
         else {
            continue;
         }
      }
      float t0 = (min[i] - ray.p0[i]) / ray.d[i]; 
      float t1 = (max[i] - ray.p0[i]) / ray.d[i];
      if (t1 < t0) {
         float tmp = t0;
         t0 = t1;
         t1 = tmp;
      }
      if (t0 > tMin) {
         tMin = t0;
      }
      if (t1 < tMax) {
         tMax = t1;
      }
   }

   if (tMin > tMax) {
      return -1.f; 
   }
   else {
      return (tMin >= 0) ? tMin : tMax;
   }
}

__device__ float getBoxIntersection(geom_t box, ray_t ray) {
   float tMin = INT_MIN; 
   float tMax = INT_MAX; 
   for (int i=0; i < 3; i++) {
      if (ray.d[i] == 0.f) {
         if (ray.p0[i] < box.v1[i] || ray.p0[i] > box.v2[i]) {
            return -1.f;
         }
         else {
            continue;
         }
      }
      float t0 = (box.v1[i] - ray.p0[i]) / ray.d[i]; 
      float t1 = (box.v2[i] - ray.p0[i]) / ray.d[i];
      if (t1 < t0) {
         float tmp = t0;
         t0 = t1;
         t1 = tmp;
      }
      if (t0 > tMin) { tMin = t0; }
      if (t1 < tMax) { tMax = t1; }
   }

   if (tMin > tMax) {
      return -1.f; 
   }
   else {
      return tMin;
   }
}

/**
 * Lighting calculation funtions - Phong-Blinn
 */

__device__ vec3 getShading(int shpIndex, gd_t gdata, ld_t ldata, ray_t ray, float t) {
   geom_t shp = gdata.geom[shpIndex];
   vec3 rpos = ray.p0 + t * ray.d;
   vec3 sCol = vec3(shp.col.x, shp.col.y, shp.col.z);
   vec3 color = shp.finish.a * sCol;
   vec3 N = getNormal(rpos, shp);
   for (int i=0; i < ldata.count; i++) {
      ray_t lightRay = {rpos, glm::normalize(ldata.lights[i].pos - rpos)}; 
      lightRay.p0 += EPSILON * lightRay.d;
      if (findIntersections(gdata, lightRay, length(ldata.lights[i].pos - rpos)).t >= 0.f) {
         continue;
      }
      vec3 L = lightRay.d;
      vec3 lCol = vec3(ldata.lights[i].color);
      vec3 V = -(ray.d); 
      vec3 H = glm::normalize(L + V); 
      float diff = shp.finish.d * clamp(glm::dot(N,L),0.0f,1.0f);
      float spec;
      if (gdata.shadeType == BLINN)
         spec = shp.finish.s * clamp(pow(max(0.0f,glm::dot(H,N)),(1.0f / shp.finish.rou)),0.0f,1.0f);
      if (gdata.shadeType == GAUSSIAN)
         spec = shp.finish.s * pow(E_CONSTANT,-(pow(clamp(acos(glm::dot(N,H)),0.0,1.0) / shp.finish.rou, 2.0f)));
      color += lCol * (sCol * diff +  spec);
   } 
   return color;
}

/**
 * Ray calculation
 */

__device__ ray_t getRay(camera_t camera, int rx, int ry, int width, int height, curandState *state) {
   vec3 w = glm::normalize(camera.look - camera.pos); 
   vec3 u = glm::normalize(glm::cross(w,camera.up));
   vec3 v = glm::normalize(glm::cross(w,u));

   vec3 ic = camera.pos + 1.0f * w; //image center
   
   float aspect = width / height;
   float H = glm::length(camera.up);
   float W = glm::length(camera.right);

   float rxd = (float)rx / SAMPLE_SIZE;
   float ryd = (float)ry / SAMPLE_SIZE;
   rxd += (curand(state) % 1000) / (2000.f * SAMPLE_SIZE);
   ryd += (curand(state) % 1000) / (2000.f * SAMPLE_SIZE);
   
   float ix = W * ((rxd / (float)width) - 0.5f); //distance along x of image plane 
   float iy = H * ((ryd / (float)height) - 0.5f); //... along y of image plane

   vec3 point = ic + ix * u + iy * v; 

   ray_t ray = { point, glm::normalize(point-camera.pos) };

   return ray; 
}

/**
 * Kernel programs
 */

__global__ void calcPixel(gd_t gdata, ld_t ldata, camera_t camera, photon_t *photons, 
      vec4 *pixels_out, int width, int height, curandState *states) {
   int rx = (blockIdx.x * BLOCK_SIZE + threadIdx.x);
   int ry = (blockIdx.y * BLOCK_SIZE + threadIdx.y); 
   int x = rx / SAMPLE_SIZE;
   int y = ry / SAMPLE_SIZE;

   if ( x > width-1 || y > height-1) {
      return;
   }

   curandState *state = &states[(ry % SAMPLE_SIZE) * SAMPLE_SIZE + (rx % SAMPLE_SIZE)];
   ray_t ray = getRay(camera, rx, ry, width, height, state);

   vec4 colorVec = vec4(rayColor<0>(gdata, ldata, ray), 1.f);
   colorVec = vec4(0.f);
   hit_t hit = findIntersections(gdata, ray, -1.f);
   vec3 poi = ray.p0 + hit.t * ray.d;
   for (int i=0; i < PHOTON_COUNT * ldata.count; i++) {
      if (abs(poi.x - photons[i].pos.x) < 2.05 &&
          abs(poi.y - photons[i].pos.y) < 2.05 &&
          abs(poi.z - photons[i].pos.z) < 2.05) {
         //float weight = (1.f - sqrt(glm::length(poi - photons[i].pos))) / 50.f;
         float weight = 15.f / PHOTON_COUNT;
         colorVec += weight * vec4(photons[i].col, 0);
      }
   }

   colorVec.x = clamp(colorVec.x, 0.f, 1.f);
   colorVec.y = clamp(colorVec.y, 0.f, 1.f);
   colorVec.z = clamp(colorVec.z, 0.f, 1.f);

   int index = (height - y - 1) * width + x;
   float weight = (float)(SAMPLE_SIZE * SAMPLE_SIZE);
   atomicAdd(&pixels_out[index].x, colorVec.x / weight);
   atomicAdd(&pixels_out[index].y, colorVec.y / weight);
   atomicAdd(&pixels_out[index].z, colorVec.z / weight);
   atomicAdd(&pixels_out[index].w, colorVec.w / weight);
}

__global__ void setupCurand(curandState *state, unsigned long seed) {
   int idx = (blockIdx.x * BLOCK_SIZE + threadIdx.x);
   int idy = (blockIdx.y * BLOCK_SIZE + threadIdx.y); 
   int id = idy * BLOCK_SIZE + idx;
   curand_init(seed, id, id, &state[id]);
}

__global__ void emitPhotons(gd_t gdata, ld_t ldata, photon_t *photons) {
   int pIdx = blockIdx.x * BLOCK_SIZE + threadIdx.x; // photon
   int lIdx = blockIdx.y * BLOCK_SIZE + threadIdx.y; // light
   if (pIdx > PHOTON_COUNT) {
      return;
   }
   if (lIdx > ldata.count - 1) {
      return;
   }
   int id = pIdx + lIdx * PHOTON_COUNT;
   curandState state;
   curand_init(0, id, id, &state);
   vec3 dir;
   dir.x = 0.5f - curand_uniform(&state); 
   dir.y = 0.5f - curand_uniform(&state); 
   dir.z = 0.5f - curand_uniform(&state); 
   dir = glm::normalize(dir);
   light_t light = ldata.lights[lIdx];
   vec3 intensity = vec3(light.color) / (float)(PHOTON_COUNT / ldata.count);
   photon_t pho = {light.pos, dir, intensity, 0}; 
   bool absorbed = false;
   int bounceCount = 0;
   do {
      ray_t phoRay = {pho.pos, pho.dir};
      hit_t hit = findIntersections(gdata, phoRay, -1.f);
      if (hit.t >= 0.f) {
         geom_t obj = gdata.geom[hit.index];
         vec3 poi = phoRay.p0 + hit.t * phoRay.d;
         pho.pos = poi;
         
         float pd = obj.finish.d;                  // diff [0, d)
         float ps = obj.finish.s + obj.finish.d;   // spec [d, s)
         float pa = 1 - ps;                        // abs  [s, 1)
         
         int russianRoulette = curand_uniform(&state);
         if (russianRoulette < pd) {
            // Get two random numbers
            float rand1 = curand_uniform(&state);
            float rand2 = curand_uniform(&state);

            // Generate randomized phi and theta
            float phi = acos(sqrt(1.f - rand1)); 
            float theta = 2.f * PI * rand2;

            // Get vector form
            vec3 newDir = vec3(sin(theta) * cos(phi), cos(theta), sin(theta) * sin(phi));

            // Wrap it around the normal
            vec3 nv = getNormal(poi, obj);
            vec3 h = nv; 
            if (abs(h.x) <= abs(h.y) && abs(h.x) <= abs(h.z)) {
               h.x = 1.f;
            }
            else if (abs(h.y) <= abs(h.x) && abs(h.y) <= abs(h.z)) {
               h.y = 1.f;
            }
            else {
               h.z = 1.f;
            }
            vec3 nu = glm::normalize(glm::cross( h, nv));
            vec3 nw = glm::normalize(glm::cross(nu, nv));

            pho.dir = glm::normalize(newDir.x * nu + newDir.y * nv + newDir.z * nw); 
            pho.pos += pho.dir * EPSILON;
         }
         else if (russianRoulette < ps) {
            vec3 normal = getNormal(poi, obj);
            pho.dir = pho.dir - 2.0f * (glm::dot(pho.dir, normal));
            pho.pos += pho.dir * EPSILON;
         }
         else {
            absorbed = true;
         }
      }
      else {
         vec4 clr = gdata.geom[hit.index].col;
         pho.col = vec3(pho.col.x * clr.x, pho.col.y * clr.y, pho.col.z * clr.z);
         absorbed = true;
      }
      bounceCount++;
   } while (absorbed == false && bounceCount < 6);
   printf("Photon %d:\n\tpos: <%.2f, %.2f, %.2f>\n\tdir:<%.2f, %.2f, %.2f>\n\tintensity: <%.5f, %.5f, %.5f>\n\tbounces: %d\n", id, pho.pos.x, pho.pos.y, pho.pos.z, dir.x, dir.y, dir.z, pho.col.x, pho.col.y, pho.col.z, bounceCount);
   photons[id] = pho;
}

/**
 * Host-side image generation
 */
extern "C" void createImage(int width, int height, vector<geom_t> geometry, vector<bvh_node_t> bvh,
      int planesIndex, vector<light_t> lights, camera_t camera, e_shading shadeType) {

   int numShapes = geometry.size();
   int numLights = lights.size();

   geom_t *d_geom_in;
   bvh_node_t *d_bvh_in;
   light_t *d_lights_in;
   vec4 *d_pix_out;
   photon_t *d_photons;
   int raysWidth = width * SAMPLE_SIZE;
   int raysHeight = height * SAMPLE_SIZE;
   curandState *d_randStates;
   const size_t sizePix = sizeof(vec4) * width * height;
   const size_t sizeGeom = sizeof(geom_t) * numShapes;
   const size_t sizeBVH = sizeof(bvh_node_t) * bvh.size();
   const size_t sizeLight = sizeof(light_t) * numLights;
   const size_t sizePhotons = sizeof(photon_t) * PHOTON_COUNT * numLights;

   double t1_render, t2_render;
   struct timeval t_render;

   Image *image = new Image(width, height);

   /* Allocate device memory for lights and geometry */
   cudaMalloc((void **) &d_geom_in, sizeGeom);
   cudaMalloc((void **) &d_bvh_in, sizeBVH);
   cudaMalloc((void **) &d_lights_in, sizeLight);
   cudaMalloc((void **) &d_photons, sizePhotons);
   cudaMalloc((void **) &d_randStates, sizeof(curandState) * SAMPLE_SIZE * SAMPLE_SIZE);

   /* Copy geometry and light arrays to the device */
   cudaMemcpy(d_geom_in, &geometry[0], sizeGeom, cudaMemcpyHostToDevice);
   cudaMemcpy(d_bvh_in, &bvh[0], sizeBVH, cudaMemcpyHostToDevice);
   cudaMemcpy(d_lights_in, &lights[0], sizeLight, cudaMemcpyHostToDevice);

   /* Build shape and light data structs */
   gd_t gdata_in = {d_geom_in, d_bvh_in, numShapes, planesIndex, shadeType};
   ld_t ldata_in = {d_lights_in, numLights};

   /* Allocate a pixel output array on the device */
   cudaMalloc((void **) &d_pix_out, sizePix);
   cudaMemset(d_pix_out, 0, sizePix);
   cudaMemset(d_photons, 0, sizePhotons);

   const dim3 pho_blocks(PHOTON_COUNT / PHOTON_BLOCK_WIDTH + ((PHOTON_COUNT % PHOTON_BLOCK_WIDTH > 0) ? 1: 0),
            numLights / PHOTON_BLOCK_HEIGHT + ((numLights % PHOTON_BLOCK_HEIGHT > 0) ? 1 : 0), 1);
   const dim3 pho_threads(PHOTON_BLOCK_WIDTH, PHOTON_BLOCK_HEIGHT, 1); 
   printf("PHO_BLOCKS: (%d,%d)\t PHO_THREADS: (%d,%d)\n", pho_blocks.x, pho_blocks.y, pho_threads.x, pho_threads.y);
   
   emitPhotons<<<pho_blocks,pho_threads>>>(gdata_in, ldata_in, d_photons);

   vector<photon_t> photons;
   photons.resize(PHOTON_COUNT * numLights);
   cudaMemcpy(&photons[0], d_photons, sizePhotons, cudaMemcpyDeviceToHost);
   KDTreeBuilder *treeBuilder = new KDTreeBuilder();
   vector<kd_node_t> kdTree = treeBuilder->buildKDTree(photons);

   const dim3 ray_blocks(raysWidth / BLOCK_SIZE + ((raysWidth % BLOCK_SIZE > 0) ? 1 : 0), 
         raysHeight / BLOCK_SIZE + ((raysHeight % BLOCK_SIZE > 0) ? 1 : 0), 1); 
   const dim3 ray_threads(BLOCK_SIZE, BLOCK_SIZE, 1); 

   gettimeofday(&t_render, NULL);  
   t1_render = t_render.tv_sec+(t_render.tv_usec/1000000.0);  

   setupCurand<<<1, (SAMPLE_SIZE, SAMPLE_SIZE)>>>(d_randStates, 1234);
   calcPixel<<<ray_blocks,ray_threads>>>(gdata_in, ldata_in, camera, d_photons, d_pix_out, width, height, d_randStates);
   cudaDeviceSynchronize();

   cudaError_t errSync  = cudaGetLastError();
   cudaError_t errAsync = cudaDeviceSynchronize();
   if (errSync != cudaSuccess) 
      printf("Sync kernel error: %s\n", cudaGetErrorString(errSync));
   if (errAsync != cudaSuccess)
      printf("Async kernel error: %s\n", cudaGetErrorString(errAsync));
   
   gettimeofday(&t_render, NULL);  
   t2_render = t_render.tv_sec+(t_render.tv_usec/1000000.0);  

   cudaMemcpy(image->_pixmap, d_pix_out, sizePix, cudaMemcpyDeviceToHost);
   image->WriteTga("output.tga",true);

   cudaFree(d_photons);
   cudaFree(d_pix_out);

   printf("\nRender time: %.6lf s\n", t2_render-t1_render);
}
