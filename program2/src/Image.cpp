/**
 * Bob Somers
 * rsomers@calpoly.edu
 * CPE 473, Winter 2010
 * Cal Poly, San Luis Obispo
 */

#ifdef __CUDACC__
#define CUDA_CALLABLE __host__ __device__
#else
#define CUDA_CALLABLE 
#endif

#include "Image.h"
using glm::vec4;

Image::Image(int width, int height)
{
    _width = width;
    _height = height;
    _max = 1.0;

    _pixmap = (vec4 *)malloc(sizeof(vec4) * _width * _height);
}

Image::~Image()
{
    free(_pixmap);
}

void Image::WriteTga(char *outfile, bool scale_color)
{
    FILE *fp = fopen(outfile, "w");
    if (fp == NULL)
    {
        perror("ERROR: Image::WriteTga() failed to open file for writing!\n");
        exit(EXIT_FAILURE);
    }
    
    // write 24-bit uncompressed targa header
    // thanks to Paul Bourke (http://local.wasp.uwa.edu.au/~pbourke/dataformats/tga/)
    putc(0, fp);
    putc(0, fp);
    
    putc(2, fp); // type is uncompressed RGB
    
    putc(0, fp);
    putc(0, fp);
    putc(0, fp);
    putc(0, fp);
    putc(0, fp);
    
    putc(0, fp); // x origin, low byte
    putc(0, fp); // x origin, high byte
    
    putc(0, fp); // y origin, low byte
    putc(0, fp); // y origin, high byte

    putc(_width & 0xff, fp); // width, low byte
    putc((_width & 0xff00) >> 8, fp); // width, high byte

    putc(_height & 0xff, fp); // height, low byte
    putc((_height & 0xff00) >> 8, fp); // height, high byte

    putc(24, fp); // 24-bit color depth

    putc(0, fp);

    // write the raw pixel data in groups of 3 bytes (BGR order)
    for (int y = 0; y < _height; y++)
    {
        for (int x = 0; x < _width; x++)
        {
            // if color scaling is on, scale 0.0 -> _max as a 0 -> 255 unsigned byte
            unsigned char rbyte, gbyte, bbyte;
            if (scale_color)
            {
                rbyte = (unsigned char)((_pixmap[x + _width * y].x / _max) * 255);
                gbyte = (unsigned char)((_pixmap[x + _width * y].y / _max) * 255);
                bbyte = (unsigned char)((_pixmap[x + _width * y].z / _max) * 255);
            }
            else
            {
                double r = (_pixmap[x + _width * y].x > 1.0) ? 1.0 : _pixmap[x + _width * y].x;
                double g = (_pixmap[x + _width * y].y > 1.0) ? 1.0 : _pixmap[x + _width * y].y;
                double b = (_pixmap[x + _width * y].z > 1.0) ? 1.0 : _pixmap[x + _width * y].z;
                rbyte = (unsigned char)(r * 255);
                gbyte = (unsigned char)(g * 255);
                bbyte = (unsigned char)(b * 255);
            }
            putc(bbyte, fp);
            putc(gbyte, fp);
            putc(rbyte, fp);
        }
    }

    fclose(fp);
}

vec4 Image::pixel(int x, int y)
{
    if (x < 0 || x > _width - 1 ||
        y < 0 || y > _height - 1)
    {
        // catostrophically fail
        fprintf(stderr, "ERROR: Image::pixel(%d, %d) outside range of the image!\n", x, y);
        exit(EXIT_FAILURE);
    }
    
    return _pixmap[x + _width * y];
}

void Image::pixel(int x, int y, vec4 pxl)
{
    if (x < 0 || x > _width - 1 ||
        y < 0 || y > _height - 1)
    {
        // catostrophically fail
        fprintf(stderr, "ERROR: Image::pixel(%d, %d, pixel) outside range of the image!\n", x, y);
        exit(EXIT_FAILURE);
    }
    
    _pixmap[x + _width * y] = pxl;

    // update the max color if necessary
    _max = (pxl.x > _max) ? pxl.x : _max;
    _max = (pxl.y > _max) ? pxl.y : _max;
    _max = (pxl.z > _max) ? pxl.z : _max;
}
