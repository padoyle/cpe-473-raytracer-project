/*
 * Shapes.cpp
 *
 * Data objects for each of the supported shapes in the povray format
 */

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Shapes.h"
#include "types.h"
#include <string>
#include <fstream>
#include <iostream>

#define MAX_IGNORE 256

Box::Box() {
   pos = vec3(0);
   pos2 = vec3(1);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
}

void Box::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);
   
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos2 = vec3(a, b, c);

   readModifiers(input);
}

geom_t Box::getStruct() {
   geom_t box;
   box.shape = BOX;
   box.v1 = pos;
   box.v2 = pos2;
   box.v3 = vec3(0);
   box.s1 = 0;
   box.s2 = 0;
   box.col = col;
   box.finish = finish.getStruct();
   box.invTrans = modelMatrix;

   return box;
}

bvh_node_t Box::getBVHNode() {
   bvh_node_t node = {pos, pos2, -1, -1, -1};
   return getTransformedNode(node);
}

Sphere::Sphere() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   rad = 1;
}

void Sphere::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(0);
   modelMatrix = translate(mat4(1),vec3(a, b, c)) * modelMatrix; 

   input.ignore(MAX_IGNORE,(int)',');
   input >> rad;

   readModifiers(input);
}

geom_t Sphere::getStruct() {
   geom_t sphere;
   sphere.shape = SPHERE;
   sphere.v1 = pos;
   sphere.v2 = vec3(0);
   sphere.v3 = vec3(0);
   sphere.s1 = rad;
   sphere.s2 = 0;
   sphere.col = col;
   sphere.finish = finish.getStruct();
   sphere.invTrans = modelMatrix;

   return sphere;
}

bvh_node_t Sphere::getBVHNode() {
   bvh_node_t node = {vec3(-rad), vec3(rad), -1, -1, -1};
   return getTransformedNode(node);
}

Cone::Cone() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   r1 = 1;
   pos2 = vec3(0);
   r2 = 1;
}

void Cone::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);
   input.ignore(MAX_IGNORE,(int)',');

   input >> r1;
   input.ignore(MAX_IGNORE,(int)',');
   
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos2 = vec3(a, b, c);
   input.ignore(MAX_IGNORE,(int)',');

   input >> r2;

   readModifiers(input);
}

geom_t Cone::getStruct() {
   geom_t cone;
   cone.shape = CONE;
   cone.v1 = pos;
   cone.v2 = pos2;
   cone.v3 = vec3(0);
   cone.s1 = r1;
   cone.s2 = r2;
   cone.col = col;
   cone.finish = finish.getStruct();
   cone.invTrans = modelMatrix;
   
   return cone;
}

bvh_node_t Cone::getBVHNode() {
   bvh_node_t node = {vec3(0), vec3(0), -1, -1, -1};

   return node;
}

Plane::Plane() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   norm = vec3(1);
   d = 0;
}

void Plane::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   norm = vec3(a, b, c);
   input.ignore(MAX_IGNORE,(int)',');

   input >> d;

   readModifiers(input);
}

geom_t Plane::getStruct() {
   geom_t plane;
   plane.shape = PLANE;
   plane.v1 = norm;
   plane.v2 = vec3(0);
   plane.v3 = vec3(0);
   plane.s1 = d;
   plane.s2 = 0;
   plane.col = col;
   plane.finish = finish.getStruct();
   plane.invTrans = modelMatrix;

   return plane;
}

bvh_node_t Plane::getBVHNode() {
   bvh_node_t node = {vec3(0), vec3(0), -1, -1, -1};

   return node;
}

Triangle::Triangle() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   pos2 = vec3(0);
   pos3 = vec3(0);
}

void Triangle::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);

   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos2 = vec3(a, b, c);

   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos3 = vec3(a, b, c);

   readModifiers(input);
}

geom_t Triangle::getStruct() {
   geom_t triangle;
   triangle.shape = TRIANGLE;
   triangle.v1 = pos;
   triangle.v2 = pos2;
   triangle.v3 = pos3;
   triangle.s1 = 0;
   triangle.s2 = 0;
   triangle.col = col;
   triangle.finish = finish.getStruct();
   triangle.invTrans = modelMatrix;

   return triangle;
}

bvh_node_t Triangle::getBVHNode() {
   bvh_node_t node = {vec3(0), vec3(0), -1, -1, -1};
   mat4 trans = glm::inverse(modelMatrix);
   vec3 v1t = vec3(trans * vec4(pos, 1.f));
   vec3 v2t = vec3(trans * vec4(pos2, 1.f));
   vec3 v3t = vec3(trans * vec4(pos3, 1.f));
   // Iterate over dimensions
   for (int i=0; i < 3; i++) {
      // Get minimum for this dim
      node.bbMin[i] = v1t[i];            
      if (v2t[i] < node.bbMin[i])
         node.bbMin[i] = v2t[i];
      if (v3t[i] < node.bbMin[i])
         node.bbMin[i] = v3t[i];

      // Get minimum for this dim
      node.bbMax[i] = v1t[i];            
      if (v2t[i] > node.bbMax[i])
         node.bbMax[i] = v2t[i];
      if (v3t[i] > node.bbMax[i])
         node.bbMax[i] = v3t[i];
   } 

   return node;
}
