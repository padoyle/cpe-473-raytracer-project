/*
 * SceneObj.h
 *
 * Base objects to represent any scene object
 */

#ifndef _SCENE_OBJ_H_
#define _SCENE_OBJ_H_

#include "glm/glm.hpp"
#include "Finish.h"
#include "types.h"

using namespace glm;
using namespace std;

class SceneObj {
   public: 
      vec3 pos;
      SceneObj();
};

class GeomObj : public SceneObj {
   public:
      Finish finish;       
      vec4 col; 
      mat4 modelMatrix;
      GeomObj();
      void readModifiers(ifstream &input);
      void printModelMatrix();
      bvh_node_t getTransformedNode(bvh_node_t node);
      virtual geom_t getStruct() { geom_t geom; return geom; }
      virtual bvh_node_t getBVHNode() { bvh_node_t node; return node; }
};

class LightSource : public SceneObj {
   public:
      vec4 col; 
      LightSource();
      void read(ifstream &input);
      light_t getStruct();
};

class Camera : public SceneObj {
   public:
      vec3 up;
      vec3 right;
      vec3 lookAt;
      Camera();
      void read(ifstream &input);
      camera_t getStruct();
};

#endif
