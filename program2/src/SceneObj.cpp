/*
 * SceneObj.cpp
 *
 * Base objects to represent any scene object
 */

#include <vector>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "SceneObj.h"
#include "Finish.h"

#define MAX_IGNORE 256
#define PI 3.14159

using namespace glm;
using namespace std;

SceneObj::SceneObj() {
   pos = vec3(0);
}

GeomObj::GeomObj() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
}

void GeomObj::printModelMatrix() {
   mat4 m = modelMatrix;
   cout << "\tmodel matrix:" << endl;
   printf("\t\t[%2.2f,\t %2.2f,\t %2.2f,\t %2.2f]\n", m[0].x, m[1].x, m[2].x, m[3].x);
   printf("\t\t|%2.2f,\t %2.2f,\t %2.2f,\t %2.2f|\n", m[0].y, m[1].y, m[2].y, m[3].y);
   printf("\t\t|%2.2f,\t %2.2f,\t %2.2f,\t %2.2f|\n", m[0].z, m[1].z, m[2].z, m[3].z);
   printf("\t\t[%2.2f,\t %2.2f,\t %2.2f,\t %2.2f]\n", m[0].w, m[1].w, m[2].w, m[3].w);
}

void GeomObj::readModifiers(ifstream &input) {
   float a, b, c;
   string word;
   input >> word;
   while (word.find('}') == string::npos) {
      if (word.compare("pigment") == 0) {
         input.ignore(MAX_IGNORE,(int)'{');
         input >> word; //ignore the "color" flag
         input >> word;

         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c;
         col = vec4(a, b, c, 1);
         if (word.compare("rgbf") == 0) {
            input.ignore(MAX_IGNORE,(int)',');
            input >> col.w;
         } 
         input.ignore(MAX_IGNORE,(int)'>');
         input.ignore(MAX_IGNORE,(int)'}');
      }
      else if (word.compare("finish") == 0) {
         finish.read(input);
      }
      else if (word.compare("translate") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         modelMatrix = translate(mat4(1),vec3(a, b, c)) * modelMatrix; 
      }
      else if (word.compare("scale") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         modelMatrix = scale(mat4(1),vec3(a, b, c)) * modelMatrix; 
      }
      else if (word.compare("rotate") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         float angle = length(vec3(a,b,c));
         if (angle != 0) {
            vec3 axis = normalize(vec3(a,b,c));
            modelMatrix = rotate(mat4(1),angle,axis) * modelMatrix; 
         }
         else {
         }
      }
      else {
         cout << "Unknown modifier " << word << endl;
      }
      input >> word;
   }
   modelMatrix = glm::inverse(modelMatrix);
}

bvh_node_t GeomObj::getTransformedNode(bvh_node_t node) {
   mat4 trans = glm::inverse(modelMatrix);
   vec3 mn = node.bbMin;
   vec3 mx = node.bbMax;

   vector<vec3> points;
   points.push_back(vec3(trans * vec4(mn, 1.f)));
   points.push_back(vec3(trans * vec4(mn.x, mn.y, mx.z, 1.f)));
   points.push_back(vec3(trans * vec4(mn.x, mx.y, mn.z, 1.f)));
   points.push_back(vec3(trans * vec4(mx.x, mn.y, mn.z, 1.f)));
   points.push_back(vec3(trans * vec4(mn.x, mx.y, mx.z, 1.f)));
   points.push_back(vec3(trans * vec4(mx.x, mn.y, mx.z, 1.f)));
   points.push_back(vec3(trans * vec4(mx.x, mx.y, mn.z, 1.f)));
   points.push_back(vec3(trans * vec4(mx, 1.f)));

   for (int dim=0; dim < 3; dim++) {
      node.bbMin[dim] = points[0][dim];
      node.bbMax[dim] = points[0][dim];
      for (int i=1; i < points.size(); i++) {
         if (points[i][dim] < node.bbMin[dim])
           node.bbMin[dim] = points[i][dim]; 
         if (points[i][dim] > node.bbMax[dim])
           node.bbMax[dim] = points[i][dim]; 
      } 
   }

   bvh_node_t newNode = {node.bbMin, node.bbMax, -1, -1, -1};
   return newNode;
}

LightSource::LightSource() {
   pos = vec3(0);
   col = vec4(1);
}

void LightSource::read(ifstream &input) {
   float a, b, c;
   string word;
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a,b,c);

   input >> word; // color 
   while (word.compare("color") != 0) {
      input >> word;
   }
   input >> word;
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c;
   col = vec4(a,b,c,1);

   if (word.compare("rgbf") == 0) {
      input.ignore(MAX_IGNORE,(int)',');
      input >> col.w;     
   } 

   input.ignore(MAX_IGNORE,(int)'>');
   input.ignore(MAX_IGNORE,(int)'}');
}

light_t LightSource::getStruct() {
   light_t light;
   light.pos = pos;
   light.color = col; 

   return light;
}

Camera::Camera() {
   pos = vec3(0);
   up = vec3(0,1,0);
   right = vec3(1,0,0);
   lookAt = vec3(0,0,1);
}

void Camera::read(ifstream &input) {
   float a, b, c;
   string word;
   input >> word;
   while (word.find('}') == string::npos) {
      if (word.compare("location") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         pos = vec3(a,b,c);
      }
      else if (word.compare("up") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         up = vec3(a,b,c);
      }
      else if (word.compare("right") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         right = vec3(a,b,c);
      }
      else if (word.compare("look_at") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         lookAt = vec3(a,b,c);
      }
      input >> word;
   }
}

camera_t Camera::getStruct() {
   camera_t camera;
   camera.pos = pos;
   camera.up = up;
   camera.right = right;
   camera.look = lookAt; 

   return camera;
}
