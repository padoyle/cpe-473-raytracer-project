/**
 * BVH
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>

#include "types.h"

static vector<photon_t> photons;

class KDTreeBuilder {
   public:
      vector<kd_node_t> buildKDTree(const vector<photon_t>& photonList); 
      void printTree(const vector<kd_node_t>& tree, int index, int depth);

   private:
      template <int dim> bool comparePhotons(kd_node_t a, kd_node_t b); 
      int buildSubtree(vector<kd_node_t>& treeSoFar, vector<kd_node_t>& nodes, int depth);
      void printNode(kd_node_t node, int ndx); 
};
