/*
 * Finish.cpp
 *
 * Defines data type to represent the surface material of an object
 */

#include "Finish.h"
#include "types.h"

#define MAX_IGNORE 256

Finish::Finish() {
   a = 0.1;
   d = 0.6;
   s = 0.0;
   rou = 0.05;
   rfl = 0.0;
   rfr = false;
   ior = 1.0;
}

Finish::~Finish() { }

void Finish::read(ifstream &input) {
   input.ignore(MAX_IGNORE,(int)'{');
   
   string word;
   input >> word;
   while (word.find("}") == string::npos) {
      if (word.compare("ambient") == 0) {
         input >> a;
      } 
      else if (word.compare("diffuse") == 0) {
         input >> d;
      } 
      else if (word.compare("specular") == 0) {
         input >> s;
      } 
      else if (word.compare("roughness") == 0) {
         input >> rou;
      } 
      else if (word.compare("reflection") == 0) {
         input >> rfl;
      } 
      else if (word.compare("refraction") == 0) {
         input >> rfr;
      } 
      else if (word.compare("ior") == 0) {
         input >> ior;
      } 
      else {
         cout << "Unknown finish parameter " << word << endl;
      }

      input >> word;
   }
}

finish_t Finish::getStruct() {
   finish_t finish;
   finish.a = a;
   finish.d = d; 
   finish.s = s;
   finish.rou = rou;
   finish.rfl = rfl;
   finish.rfr = rfr;
   finish.ior = ior;

   return finish;
}
