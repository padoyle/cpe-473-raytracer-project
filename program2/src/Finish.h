/*
 * Finish.h
 *
 * Defines data type to represent the surface material of an object
 */

#ifndef _FINISH_H_
#define _FINISH_H_

#include <string>
#include <fstream>
#include <iostream>

typedef struct finish_struct {
   float a;
   float d;
   float s;
   float rou;
   float rfl;
   bool rfr;
   float ior;
} finish_t;

using namespace std;

class Finish {
   public: 
      float a;
      float d;
      float s;
      float rou;
      float rfl;
      bool rfr;
      float ior;
      Finish();
      ~Finish();
      void read(ifstream &input);
      finish_t getStruct();     
};
#endif
