/**
 * KD Tree builder
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <vector>

#include "types.h"
#include "KDTreeBuilder.h"

bool photonCompareX (const kd_node_t& a, const kd_node_t& b) {
   return (photons[a.photonId].pos[0] < photons[b.photonId].pos[0]);
}

bool photonCompareY (const kd_node_t& a, const kd_node_t& b) {
   return (photons[a.photonId].pos[1] < photons[b.photonId].pos[1]);
}

bool photonCompareZ (const kd_node_t& a, const kd_node_t& b) {
   return (photons[a.photonId].pos[2] < photons[b.photonId].pos[2]);
}

vector<kd_node_t> KDTreeBuilder::buildKDTree(const vector<photon_t>& photonList) {
   photons = photonList;
   vector<kd_node_t> kdTree;
   vector<kd_node_t> nodesList;
   // Build list of nodes
   for (int i=0; i < photons.size(); i++) {
      kd_node_t node = {-1, i, -1, -1};
      nodesList.push_back(node);
   }

   printf("Launch recursive tree construction with %d nodes\n", nodesList.size());
   buildSubtree(kdTree, nodesList, 0);
   printTree(kdTree, 0, 0);

   return kdTree;
}

int KDTreeBuilder::buildSubtree(vector<kd_node_t>& treeSoFar, vector<kd_node_t>& nodes, int depth) {
   int myIndex = treeSoFar.size();
   int nextDim = depth % 3;
  
   if (nodes.size() == 0) {
      return -1;
   }
   
   if (nodes.size() == 1) {
      nodes[0].dim = nextDim;
      treeSoFar.push_back(nodes[0]);
      return myIndex;
   } 

   switch (nextDim) {
      case 0:
         sort(nodes.begin(), nodes.end(), photonCompareX);
         break;
      case 1:
         sort(nodes.begin(), nodes.end(), photonCompareY);
         break;
      case 2:
         sort(nodes.begin(), nodes.end(), photonCompareZ);
         break;
   }
   int numNodes = nodes.size();
   kd_node_t myNode = nodes[numNodes/2]; 
   myNode.dim = nextDim;
   treeSoFar.push_back(myNode);

   vector<kd_node_t> leftList, rightList;
   for (int i=0; i < numNodes / 2; i++) {
      leftList.push_back(nodes[i]);
   }
   for (int i=(numNodes / 2) + 1; i < numNodes; i++) {
      rightList.push_back(nodes[i]);
   }

   myNode.left = buildSubtree(treeSoFar, leftList, depth+1);
   myNode.right = buildSubtree(treeSoFar, rightList, depth+1);
   treeSoFar[myIndex] = myNode;

   return myIndex;
}

void KDTreeBuilder::printTree(const vector<kd_node_t>& tree, int index, int depth) {
   for (int i=0; i < depth; i++) {
      printf("  ");
   }
   printNode(tree[index], index);
   if (tree[index].left != -1) {
      printTree(tree, tree[index].left, depth + 1);
   }
   if (tree[index].right != -1) {
      printTree(tree, tree[index].right, depth + 1);
   }
}

void KDTreeBuilder::printNode(kd_node_t node, int ndx) {
   photon_t pho = photons[node.photonId];
   printf("Photon %d: id = %d; pos = <%.2f, %.2f, %.2f>; left = %d; right = %d\n", ndx, node.photonId, pho.pos.x, pho.pos.y, pho.pos.z, node.left, node.right); 
}
