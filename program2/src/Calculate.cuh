/*
 * Calculate.cuh
 *
 * Perform cuda calculations for the image
 */

#ifdef __CUDACC__
#define CUDA_CALLABLE __device__
#else
#define CUDA_CALLABLE
#endif

#include "SceneObj.h"
#include "types.h"
#include <vector>

extern "C" {
   void createImage(int width, int height, vector<geom_t> geometry, vector<bvh_node_t> bvh,
      int planesIndex, vector<light_t> lights, camera_t camera, e_shading shadeType);
}
