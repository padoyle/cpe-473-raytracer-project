// cs473, assignment 1 recursion test (RIGHT HANDED)
camera {
  location  <2, 0, 10>
  up        <0,  1,  0>
  right     <1.7777778, 0,  0>
  look_at   <0, 2, 0>
}

light_source {<-30, 30, 20> color rgb <0.8, 0.8, 0.8>}
light_source {<30, 30, 20> color rgb <0.8, 0.8, 0.8>}

box {
   <-1, -1, -1>,
   <1, 1, 1>
   pigment {color rgb <0.9, 0.9, 0.1>}
   finish {ambient 0.1 diffuse 0.7 specular 0.2 roughness 0.4}
   translate <0, 0, 0>
}

box {
   <-1, -1, -1>,
   <1, 1, 1>
   pigment {color rgb <0.1, 0.9, 0.9>}
   finish {ambient 0.1 diffuse 0.7 specular 0.2 roughness 0.01}
   translate <0, 2.4, 0>
}

box {
   <-1, -1, -1>,
   <1, 1, 1>
   pigment {color rgb <0.5, 0.1, 0.9>}
   finish {ambient 0.1 diffuse 0.7 specular 0.2 roughness 0.1}
   translate <0, 4.8, 0>
}

box {
   <-1, -1, -1>,
   <1, 1, 1>
   pigment {color rgb <0.9, 0.1, 0.1>}
   finish {ambient 0.1 diffuse 0.7 specular 0.2}
   translate <-2.4, 2.4, 0>
}

box {
   <-1, -1, -1>,
   <1, 1, 1>
   pigment {color rgb <0.1, 0.9, 0.1>}
   finish {ambient 0.1 diffuse 0.7 specular 0.2}
   translate <2.4, 2.4, 0>
}

// refraction!

sphere { <0, 0, 0>, 0.666
   pigment {color rgbf <0.1, 0.1, 0.1, 1.0>}
   finish {ambient 0.0 diffuse 0.5 refraction 1 ior 1.0}
   translate <-1.2, 1.2, 1.7>
}

sphere { <0, 0, 0>, 0.666
   pigment {color rgbf <0.1, 0.1, 0.1, 1.0>}
   finish {ambient 0.0 diffuse 0.5 refraction 1 ior 1.3}
   translate <1.2, 1.2, 1.7>
}

sphere { <0, 0, 0>, 0.666
   pigment {color rgbf <0.1, 0.1, 0.1, 1.0>}
   finish {ambient 0.0 diffuse 0.5 refraction 1 ior 2.0}
   translate <-1.2, 3.6, 1.7>
}

sphere { <0, 0, 0>, 0.666
   pigment {color rgbf <0.1, 0.1, 0.1, 1.0>}
   finish {ambient 0.0 diffuse 0.5 refraction 1 ior 3.5}
   translate <1.2, 3.6, 1.7>
}

// floor
plane {<0, 1, 0>, -1.01
  pigment {color rgb <0.5, 0.5, 0.5>}
  finish {ambient 0.3 diffuse 0.5 reflection 0.7}
}

plane { <0, 0, 1>, -1
   pigment {color rgb <0.4, 0.1, 0.1>}
   finish {ambient 0.2 diffuse 0.6 reflection 0.4}
   translate <0, 0, -10>
}
