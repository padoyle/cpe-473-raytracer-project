/*
 * SceneObj.h
 *
 * Base objects to represent any scene object
 */

#ifndef _SCENE_OBJ_H_
#define _SCENE_OBJ_H_

#include "glm/glm.hpp"
#include "Finish.h"

using namespace glm;
using namespace std;

class SceneObj {
   public: 
      vec3 pos;
      SceneObj();
};

class GeomObj : public SceneObj {
   public:
      Finish finish;       
      vec4 col; 
      mat4 modelMatrix;
      GeomObj();
      void readModifiers(ifstream &input);
      void printModelMatrix();
};

class LightSource : public SceneObj {
   public:
      vec4 col; 
      LightSource();
      void read(ifstream &input);
};

class Camera : public SceneObj {
   public:
      vec3 up;
      vec3 right;
      vec3 lookAt;
      Camera();
      void read(ifstream &input);
};

#endif
