/*
 * SceneObj.cpp
 *
 * Base objects to represent any scene object
 */

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "SceneObj.h"
#include "Finish.h"

#define MAX_IGNORE 256

using namespace glm;
using namespace std;

SceneObj::SceneObj() {
   pos = vec3(0);
}

GeomObj::GeomObj() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
}

void GeomObj::printModelMatrix() {
   mat4 m = modelMatrix;
   cout << "\tmodel matrix:" << endl;
   cout << "\t\t[" << m[0].x << ", " << m[1].x << ", " << m[2].x << ", " << m[3].x << "]" << endl;
   cout << "\t\t[" << m[0].y << ", " << m[1].y << ", " << m[2].y << ", " << m[3].y << "]" << endl;
   cout << "\t\t[" << m[0].z << ", " << m[1].z << ", " << m[2].z << ", " << m[3].z << "]" << endl;
   cout << "\t\t[" << m[0].w << ", " << m[1].w << ", " << m[2].w << ", " << m[3].w << "]" << endl;
}

void GeomObj::readModifiers(ifstream &input) {
   float a, b, c;
   string word;
   input >> word;
   while (word.find('}') == string::npos) {
      if (word.compare("pigment") == 0) {
         input.ignore(MAX_IGNORE,(int)'{');
         input >> word; //ignore the "color" flag
         input >> word;

         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c;
         col = vec4(a, b, c, 1);
         if (word.compare("rgbf") == 0) {
            input.ignore(MAX_IGNORE,(int)',');
            input >> col.w;
         } 
         input.ignore(MAX_IGNORE,(int)'>');
         input.ignore(MAX_IGNORE,(int)'}');
         cout << "\tcol: " << col.x << ", " << col.y << ", " 
            << col.z << ", " << col.w << endl;
      }
      else if (word.compare("finish") == 0) {
         finish.read(input);
      }
      else if (word.compare("translate") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         modelMatrix = translate(modelMatrix,vec3(a, b, c)); 
         cout << "\ttranslate: " << a << ", " << b << ", " << c << endl; 
      }
      else if (word.compare("scale") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         modelMatrix = scale(modelMatrix,vec3(a, b, c)); 
         cout << "\tscale: " << a << ", " << b << ", " << c << endl; 
      }
      else if (word.compare("rotate") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         vec3 axis = normalize(vec3(a,b,c));
         float angle = length(vec3(a,b,c));
         modelMatrix = rotate(modelMatrix,angle,axis); 
         cout << "\trotate: " << axis.x << ", " << axis.y << ", " << axis.z << ", " << angle << endl;
      }
      else {
         cout << "Unknown modifier " << word << endl;
      }
      input >> word;
   }
}

LightSource::LightSource() {
   pos = vec3(0);
   col = vec4(1);
}

void LightSource::read(ifstream &input) {
   float a, b, c;
   string word;
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a,b,c);
   cout << "\tpos: " << a << ", " << b << ", " << c << endl;

   input >> word; // color 
   while (word.compare("color") != 0) {
      input >> word;
   }
   input >> word;
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c;
   col = vec4(a,b,c,1);

   if (word.compare("rgbf") == 0) {
      input.ignore(MAX_IGNORE,(int)',');
      input >> col.w;     
   } 
   cout << "\tcol: " << a << ", " << b << ", " << c << ", " << col.w << endl;

   input.ignore(MAX_IGNORE,(int)'>');
   input.ignore(MAX_IGNORE,(int)'}');
}

Camera::Camera() {
   pos = vec3(0);
   up = vec3(0,1,0);
   right = vec3(1,0,0);
   lookAt = vec3(0,0,1);
}

void Camera::read(ifstream &input) {
   float a, b, c;
   string word;
   input >> word;
   while (word.find('}') == string::npos) {
      if (word.compare("location") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         pos = vec3(a,b,c);
         cout << "\tlocation: " << a << ", " << b << ", " << c << endl;
      }
      else if (word.compare("up") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         up = vec3(a,b,c);
         cout << "\tup: " << a << ", " << b << ", " << c << endl;
      }
      else if (word.compare("right") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         right = vec3(a,b,c);
         cout << "\tright: " << a << ", " << b << ", " << c << endl;
      }
      else if (word.compare("look_at") == 0) {
         input.ignore(MAX_IGNORE,(int)'<');
         input >> a; input.ignore(MAX_IGNORE,(int)',');
         input >> b; input.ignore(MAX_IGNORE,(int)',');
         input >> c; input.ignore(MAX_IGNORE,(int)'>');
         lookAt = vec3(a,b,c);
         cout << "\tlook_at: " << a << ", " << b << ", " << c << endl;
      }
      input >> word;
   }
}
