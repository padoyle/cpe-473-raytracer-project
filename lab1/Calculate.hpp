/*
 * Calculate.hpp
 *
 * Perform cuda calculations for the image
 */

#include "RasterizerBase/types.h"
#include "RasterizerBase/Image.h"

__global__ void calcPixel(Image *image, float *width, float *height) {
   int x = blockIdx.x;
   int y = threadIdx.y; 

   color_t color = { (float)x / *width, (float)y / *height, 0.0f, 1.0f};
   image->pixel(x, y, color);
}

void createImage(int width, int height) {
   Image image = Image(width, height);
   Image *h_image = &image;
   Image *d_image;

   cudaMalloc((void **) &d_image, sizeof(Image));
}
