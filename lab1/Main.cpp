/*
 * Main.cpp
 *
 * Main file for povray parser
 */

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "SceneObj.h"
#include "Shapes.h"
#include "Finish.h"
#include "Calculate.cuh"
#include "Image.h"
#include "types.h"

using namespace std;

int g_width;
int g_height;

vector<SceneObj*> parseInputFile(char *filename) {
   vector<SceneObj*> objects;

   ifstream inputStream;
   inputStream.open(filename);

   if (!inputStream.is_open()) {
      printf("Could not open file %s.\n",filename);
      exit(1);
   }

   while (!inputStream.eof()) {
      string word;
      inputStream >> word;
      if (inputStream.eof()) {
         break;
      }
      if (word.substr(0, 2).compare("//") == 0) {
         inputStream.ignore(512,(int)'\n');
         cout << "Ignoring comment\n";
         continue;
      }
      else if (word.compare("camera") == 0) {
         Camera *camera = new Camera();
         cout << "Camera:" << endl;
         camera->read(inputStream);
         objects.push_back(new Camera());
      }
      else if (word.compare("light_source") == 0) {
         LightSource *lightSource = new LightSource();
         cout << "Light source:" << endl;
         lightSource->read(inputStream);
         objects.push_back(new LightSource());
      }
      else if (word.compare("box") == 0) {
         Box *box = new Box();
         cout << "Box:" << endl;
         box->read(inputStream);
         objects.push_back(box);
      }
      else if (word.compare("sphere") == 0) {
         Sphere *sphere = new Sphere();
         cout << "Sphere:" << endl;
         sphere->read(inputStream);
         objects.push_back(sphere);
      }
      else if (word.compare("cone") == 0) {
         Cone *cone = new Cone();
         cout << "Cone:" << endl;
         cone->read(inputStream);
         objects.push_back(cone);
      }
      else if (word.compare("plane") == 0) {
         Plane *plane = new Plane();
         cout << "Plane:" << endl;
         plane->read(inputStream);
         objects.push_back(plane);
      }
      else if (word.compare("triangle") == 0) {
         Triangle *triangle = new Triangle();
         cout << "Triangle:" << endl;
         triangle->read(inputStream);
         objects.push_back(triangle);
      }
      else {
         cout << "Unknown item: " << word << endl;
      } 
   }

   return objects;
}

int main(int argc, char *argv[]) {
   
   if (argc != 4) {
      printf("Unrecognized or missing arguments.  Use format:\n\traytrace <width> <height> <input file>\n"); 
      exit(1);
   }

   g_width = atoi(argv[1]);
   g_height = atoi(argv[2]);

   if (g_width <= 0 || g_height <= 0) {
      printf("Image dimensions must be positive integers\n");
      exit(1);
   }

   parseInputFile(argv[3]);

   return 0;
}
