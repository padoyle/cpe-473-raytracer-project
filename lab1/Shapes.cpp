/*
 * Shapes.cpp
 *
 * Data objects for each of the supported shapes in the povray format
 */

#include "glm/glm.hpp"

#include "Shapes.h"
#include <string>
#include <fstream>
#include <iostream>

#define MAX_IGNORE 256

Box::Box() {
   pos = vec3(0);
   pos2 = vec3(1);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
}

void Box::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);
   cout << "\tpos: " << a << ", " << b << ", " << c << endl;
   
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos2 = vec3(a, b, c);
   cout << "\tpos2: " << a << ", " << b << ", " << c << endl;

   readModifiers(input);
   printModelMatrix();
}

Sphere::Sphere() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   rad = 1;
}

void Sphere::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);
   cout << "\tpos: " << a << ", " << b << ", " << c << endl;

   input.ignore(MAX_IGNORE,(int)',');
   input >> rad;
   cout << "\trad: " << rad << endl;

   readModifiers(input);
   printModelMatrix();
}

Cone::Cone() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   r1 = 1;
   pos2 = vec3(0);
   r2 = 1;
}

void Cone::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);
   input.ignore(MAX_IGNORE,(int)',');
   cout << "\tpos: " << a << ", " << b << ", " << c << endl;

   input >> r1;
   input.ignore(MAX_IGNORE,(int)',');
   cout << "\tr1: " << r1 << endl;
   
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos2 = vec3(a, b, c);
   input.ignore(MAX_IGNORE,(int)',');
   cout << "\tpos2: " << a << ", " << b << ", " << c << endl;

   input >> r2;
   cout << "\tr2: " << r2 << endl;

   readModifiers(input);
   printModelMatrix();
}

Plane::Plane() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   norm = vec3(1);
   d = 0;
}

void Plane::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   norm = vec3(a, b, c);
   input.ignore(MAX_IGNORE,(int)',');
   cout << "\tnorm: " << a << ", " << b << ", " << c << endl;

   input >> d;
   cout << "\td: " << d << endl;

   readModifiers(input);
   printModelMatrix();
}

Triangle::Triangle() {
   pos = vec3(0);
   col = vec4(1);
   finish = Finish();
   modelMatrix = mat4(1);
   pos2 = vec3(0);
   pos3 = vec3(0);
}

void Triangle::read(ifstream &input) {
   float a, b, c;
   input.ignore(MAX_IGNORE,(int)'{');
   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos = vec3(a, b, c);
   cout << "\tpos: " << a << ", " << b << ", " << c << endl;

   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos2 = vec3(a, b, c);
   cout << "\tpos2: " << a << ", " << b << ", " << c << endl;

   input.ignore(MAX_IGNORE,(int)'<');
   input >> a; input.ignore(MAX_IGNORE,(int)',');
   input >> b; input.ignore(MAX_IGNORE,(int)',');
   input >> c; input.ignore(MAX_IGNORE,(int)'>');
   pos3 = vec3(a, b, c);
   cout << "\tpos3: " << a << ", " << b << ", " << c << endl;

   readModifiers(input);
   printModelMatrix();
}
