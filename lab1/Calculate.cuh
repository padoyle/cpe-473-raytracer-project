/*
 * Calculate.cuh
 *
 * Perform cuda calculations for the image
 */

#include "types.h"
#include "Image.h"

void createImage(int width, int height);
