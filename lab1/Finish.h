/*
 * Finish.h
 *
 * Defines data type to represent the surface material of an object
 */

#ifndef _FINISH_H_
#define _FINISH_H_

#include <string>
#include <fstream>
#include <iostream>

using namespace std;

class Finish {
   public: 
      float a;
      float d;
      float s;
      float rou;
      float rfl;
      float rfr;
      float ior;
      Finish();
      ~Finish();
      void read(ifstream &input);
};

#endif
